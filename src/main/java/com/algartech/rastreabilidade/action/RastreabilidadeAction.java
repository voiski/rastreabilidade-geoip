package com.algartech.rastreabilidade.action;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import com.algartech.rastreabilidade.form.RastreabilidadeForm;

public class RastreabilidadeAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}

		RastreabilidadeForm helloWorldForm = (RastreabilidadeForm) form;
		helloWorldForm.setMessage("IP: " + ipAddress);
		
		//http://maps.googleapis.com/maps/api/geocode/json?latlng=-19.976918899999998,-43.945084&sensor=false
		//http://www.w3schools.com/html/tryit.asp?filename=tryhtml5_geolocation_map

		return mapping.findForward("success");
	}

	private String getLatLong(String ip) throws Exception{
		String jsonContent="";
		
		URL url = new URL("http://ip-api.com/json/"+ip);
		URLConnection conn = url.openConnection();
		BufferedReader br = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
		String inputLine;
		while ((inputLine = br.readLine()) != null) {
			jsonContent+=inputLine;
		}
		
		JSONParser parser = new JSONParser();
		Object obj=parser.parse(jsonContent);
		
		return ""; 
	}
}
