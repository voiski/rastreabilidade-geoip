package com.algartech.rastreabilidade.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RastreabilidadeServlet extends HttpServlet {

    static Logger logger = Logger.getLogger("logRastreabilidade");  
    static FileHandler fh;
    
	static{
		try {
			fh = new FileHandler("logRastreabilidade.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();  
			fh.setFormatter(formatter); 
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(req.getInputStream()));
		String line = null;

		StringBuilder responseData = new StringBuilder();
		while((line = in.readLine()) != null) {
			responseData.append(line);
		}
		
		String ipAddress = req.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = req.getRemoteAddr();
		}
		
		logger.info(ipAddress+"="+responseData.toString());
	}
	
}
