package com.algartech.rastreabilidade.form;

import org.apache.struts.action.ActionForm;

public class RastreabilidadeForm extends ActionForm {

	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
