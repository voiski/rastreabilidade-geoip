<%@taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Alan Voiski">

    <title>POC Geo Location Demo</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/examples/starter-template/starter-template.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="rastreabilidade.do?tipo=0">POC Geo Location Demo</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="<%="1".equals(request.getParameter("tipo"))?"":"active"%>"><a href="rastreabilidade.do?tipo=0">Por Recurso Navegador</a></li>
            <li class="<%="1".equals(request.getParameter("tipo"))?"active":""%>"><a href="rastreabilidade.do?tipo=1">Por IP</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <div class="starter-template">
	    
        <%if("1".equals(request.getParameter("tipo"))){%>
        <h1>Geolocalização IP</h1>
        <p class="lead">
Geolocalização IP é uma técnica para identificar o endereço IP do partido sujeito, então, determina
que país, estado, cidade, CEP, organização ou localização do endereço do IP foi atribuído. Existem
vários provedores de geolocalização IP na indústria de fornecimento de bases de dados de endereços
IP. Estas bases de dados contêm dados de endereços IP que podem ser utilizados em firewalls,
servidores de nome de domínio, servidores de anúncios, roteamento, sistemas de correio, web sites,
e outros sistemas automatizados onde geolocalização podem ser úteis. Você pode visitar IP2Location.com
para uma demonstração gratuita.
        </p> 
		<%}else{%>
        <h1>W3C Geolocation API</h1>
        <p class="lead">
W3C Geolocation API é um esforço do World Wide Web Consortium (W3C) para padronizar uma interface 
para recuperar as informações de localização geográfica para um dispositivo cliente. As fontes mais 
comuns de informações de localização são o endereço IP, Wi-Fi e Bluetooth endereço MAC, de 
rádio-freqüência (RFID), Wi-Fi local da conexão, ou dispositivo de Sistema de Posicionamento Global 
(GPS) e IDs de GSM / CDMA celulares. A localização é devolvido com uma precisão dada dependendo da 
localização melhor fonte de informações disponível.
        </p>
		<%}%>
        <p id="demo"></p>
        <div class="row">
        	<div class="col-md-6">
	        	<table id="GeoResults" class="table"></table>
        	</div>
        	<div class="col-md-6">
        		<div id="mapholder"></div>
        	</div>
        </div>
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<!-- App common js -->
	<script src="js/common.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
	<script>
		<%if("1".equals(request.getParameter("tipo"))){%>
		$($.geoip.getIPLocation());
		<%}else{%>
		$($.geoip.getLocalLocation());
		<%}%>
	</script>
  </body>
</html>
