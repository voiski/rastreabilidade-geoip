$.geoip = {
	LOCAL_DEMO_PANEL : document.getElementById("demo"),
	
	//IP geolocation feature
	getIPLocation : function() {
		$.getJSON("http://ip-api.com/json/?callback=?", function(data) {
			var latlon = data.lat + "," + data.lon;
			
			$.geoip.printLocationValues(latlon);
			$.geoip.printMap(latlon);
			
			$.geoip.sendInformation({tipo:'ip',latlon:latlon,ip:data.query});
		});
	},
	
	//Local geolocation feature
	getLocalLocation : function() {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(this.showPosition, this.showError);
		} else {
			this.LOCAL_DEMO_PANEL.innerHTML = "Geolocation is not supported by this browser.";
		}
	},
	showPosition : function(position) {
		var latlon = position.coords.latitude + "," + position.coords.longitude;
		
		$.geoip.printLocationValues(latlon);
		$.geoip.printMap(latlon);
		
		$.geoip.sendInformation({tipo:'nativo',latlon:latlon,precisao:position.accuracy});
	},
	showError : function(error) {
		switch (error.code) {
		case error.PERMISSION_DENIED:
			this.LOCAL_DEMO_PANEL.innerHTML = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			this.LOCAL_DEMO_PANEL.innerHTML = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			this.LOCAL_DEMO_PANEL.innerHTML = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			this.LOCAL_DEMO_PANEL.innerHTML = "An unknown error occurred."
			break;
		}
	},
	
	//Comum
	printLocationValues: function(latlon){
		$.getJSON("https://maps.googleapis.com/maps/api/geocode/json?latlng="+latlon+"&sensor=false", function(data) {
			var currentLocation = data.results[0];
			
			var locationWrap = {
					"Latitute/Longitude":latlon,
					"Endereço Completo":currentLocation.formatted_address
			}
			$.each(currentLocation.address_components, function(k, v) {
				if($.inArray("neighborhood",v.types)){
					locationWrap["Bairro"]=v.long_name;
				}else if($.inArray("postal_code",v.types)){
					locationWrap["CEP"]=v.long_name;
				}
			});
			
			var table_body = "";
			$.each(locationWrap, function(k, v) {
				table_body += "<tr><td>" + k + "</td><td><b>" + v + "</b></td></tr>";
			});
			$("#GeoResults").html(table_body);
		});
	},
	printMap: function(latlon){
		var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="
				+ latlon + "&zoom=14&size=400x300&sensor=false&path=weight:10|"+latlon+"|"+latlon+"1";
		document.getElementById("mapholder").innerHTML = "<img src='" + img_url + "'>";
	},
	sendInformation: function(jsonObj){
		$.ajax({
		    url: 'log',
		    type: 'POST',
		    data: JSON.stringify(jsonObj),
		    contentType: 'application/json; charset=utf-8',
		    dataType: 'json',
		    async: false,
		    success: function(msg) {
		        if(console)console.log(msg)
		    }
		});
	}
};
